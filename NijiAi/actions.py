from dataclasses import dataclass


@dataclass
class BattleAction:
    Activate :int = 0
    Attack :int = 1
    ToMainPhaseTwo :int = 2
    ToEndPhase :int = 3

@dataclass
class MainAction:
    Summon :int = 0
    SpSummon :int = 1
    Repos  :int = 2
    SetMonster :int = 3
    SetSpell :int = 4
    Activate :int = 5
    ToBattlePhase :int = 6
    ToEndPhase :int = 7
    ShuffleHand  :int= 8


@dataclass
class stats:
    Nothing : int = 0
    SelectBattleCmd : int = 1
    SelectCard: int = 2
    SelectUnselectCard: int = 3
    SelectChain: int = 4
    SelectCounter: int = 5
    SelectDisfield: int = 6
    SelectEffectYn: int = 7
    SelectIdleCmd: int = 8
    SelectOption: int = 9
    SelectPlace: int = 10
    SelectPosition : int = 11
    SelectSum : int = 12
    SelectTribute : int = 13
    AnnounceAttribute : int = 14
    AnnounceCard : int = 15
    AnnounceNumber : int = 16
    AnnounceRace : int = 17
    AnnounceCardFilter : int = 18
    WaitingForDeck : int = 19
    SelectYesNo : int = 20
    RockPaperScissors : int = 21
    SortCard  : int = 22

