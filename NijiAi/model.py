from typing import List, Optional
from pydantic import BaseModel
import ujson
from dataclasses import asdict, astuple, dataclass, fields


class IdleCommandState(BaseModel):
    CurrentStateCode: int
    CurrentStateName: str


class CardCoordinate(BaseModel):
    Controller: int
    Location: int
    Sequence: int


class ActivationCardIndex(BaseModel):
    ActionDescription: int
    ActionIndex: int


class DynamicCard(BaseModel):
    ID: int
    Alias: int
    Type: int
    Level: int
    Rank: int
    LScale: int
    RScale: int
    LinkMarker: int
    Attribute: int
    Race: int
    Attack: int
    BaseAttack: int
    Defense: int
    BaseDefense: int
    CurrentCoordinates: CardCoordinate
    Position: int
    Owner: int
    SelectSeq: int
    OpParam1: int
    OpParam2: int
    Reason: int
    ActionIndex: List[int]
    ActionActivateIndex: List[ActivationCardIndex]
    ReasonCard: CardCoordinate
    EquipTarget: CardCoordinate
    Negator: CardCoordinate
    Negating: CardCoordinate
    EquipCards: List[CardCoordinate]
    OwnTargets: List[CardCoordinate]
    ChainOnlyOwnTargets: List[CardCoordinate]
    TargetCards: List[CardCoordinate]
    CurrentChainTargets: List[CardCoordinate]
    Overlays: List[int]
    AdditionalDescs: List[int]
    AdditionalTexts: List[str]
    Counters: Optional[List[int]]
    CanDirectAttack: bool
    Attacked: bool
    IsSpecialSummoned: bool
    Status: int


class IdleCmdStateData(BaseModel):
    SummonableCards: List[DynamicCard]
    SpecialSummonableCards: List[DynamicCard]
    ReposableCards: List[DynamicCard]
    MonsterSetableCards: List[DynamicCard]
    SpellSetableCards: List[DynamicCard]
    ActivableCards: List[DynamicCard]
    ActivableDescriptions: List[int]
    CanBattlePhase: bool
    CanEndPhase: bool
    CanShuffleHand: bool


class IdleState(BaseModel):
    CurrentStateHeader: IdleCommandState
    CurrentStateData: IdleCmdStateData

    class Config:
        json_loads = ujson.loads
        json_dumps = ujson.dumps


@dataclass
class BotDetails():
    BotID: int
    DeckName: str
    DeckIndex: int
    DeckHash: str


@dataclass
class BotUrls:
    BaseUrl: str
    BotID: int

    def __post_init__(self):
        self.BotState = f"{self.BaseUrl}/get-bot-state/{self.BotID}"
        self.BotStateHeaders = f"{self.BaseUrl}/get-bot-state-header/{self.BotID}"
        self.SetBotResponse = f"{self.BaseUrl}/set-bot-response/{self.BotID}"
        self.BotList = f"{self.BaseUrl}/bot-list"
        self.SetBotData = f"{self.BaseUrl}/set-bot-data/{self.BotID}"
        self.BotFieldInfo = f"{self.BaseUrl}/bot-field-info"
        self.FieldInfo = f"{self.BaseUrl}/field-info"
        self.FieldCardInfo = f"{self.BaseUrl}/field-card-info"
        self.BotFieldCardInfo = f"{self.BaseUrl}/bot-field-card-info"
        self.CurrentDeck = f"{self.BaseUrl}/current-deck"


def is_available(zones, zone_id):
    return (zones & zone_id) > 0


@dataclass
class Zones:
    z1: hex = 0x2
    z2: hex = 0x4
    z3: hex = 0x8
    z4: hex = 0x10
    z5: hex = 0x20
    z6: hex = 0x40
    MonsterZones: hex = 0x7f
    MainMonsterZones: hex = 0x1f
    ExtraMonsterZones: hex = 0x60
    SpellZones: hex = 0x1f
    LeftPendulumZone: hex = 0x1
    RightPendulumZone: hex = 0x2
    PendulumZones: hex = 0x3
    LinkedZones: hex = 0x10000
    NotLinkedZones: hex = 0x20000

    def available_zones(self, zone_filter):
        d = list(astuple(self))[:4]
        r = []
        for j in d:
            if is_available(zone_filter, j):
                r.append(j)
        return r
