
from model import DynamicCard, Zones
from server import Server
import asyncio
from difflib import get_close_matches
from contextlib import suppress
import logging
from actions import MainAction,stats
from errors import WrongStateError
from base64 import b64encode
from struct import pack

class Bot(object):
    def __init__(self, deck="mirror") -> None:
        super().__init__()
        self.server = Server()
        try:
            self.is_created = self.server.create_bot(deck)
        except Exception as e:
            self.is_created = False
            logging.error(f"Exception occurred {e}")
            raise RuntimeError("Please add AI_Custom bot in duel first!")
        self.state = self.server.new_state
        self.event_string = ["on_idle"]
        self.events = {}
        self.data = None
        self.zones = Zones()
        self.possible_stats = stats()

    def register(self, event: str):
        def inner(func):
            if event not in self.event_string:
                m = get_close_matches(event, self.event_string)
                a = f"Do you mean '{m[0]}' ?" if m else ""
                raise NameError(f"Event '{event}' doesn't exist!{a}")
            self.events[event] = func

        return inner

    async def handle_idle_event(self):
        self.data = self.server.get_state()

    async def emit_event(self, event):
        with suppress(KeyError): asyncio.create_task(self.events[event]())

    async def summon(self, card: DynamicCard, location="auto"):
        payload = pack("hh", MainAction.Summon, card.ActionIndex[MainAction.Summon])
        en = b64encode(payload).decode("utf8")
        return self.server.summon_card(en, location)

    async def to_battlephase(self):
        if self.state.CurrentStateCode == self.possible_stats.SelectIdleCmd:
            payload = pack("hh",MainAction.ToBattlePhase,0)
            en = b64encode(payload).decode("utf8")
            return self.server.run_aux_command(en)
        else:
            raise WrongStateError("Battle Phase only possible in Main Phase 1")


    async def run(self):
        while True:
            command = self.server.get_idle_cmd()
            match command.CurrentStateCode:
                case self.state.CurrentStateCode:
                    await asyncio.sleep(1)
                    continue

                case self.possible_stats.Nothing:
                    await asyncio.sleep(1)
                    continue

                case self.possible_stats.SelectIdleCmd :
                    self.state = command
                    await self.handle_idle_event()
                    await self.emit_event("on_idle")
                    await asyncio.sleep(1)
                    
                case self.possible_stats.SelectPlace:
                    self.state = command
                    await self.handle_idle_event()
                    await self.emit_event("on_place")
                    await asyncio.sleep(1)
                case _:
                    logging.warning(f"{command.CurrentStateName} event skipped!")
                    await asyncio.sleep(1)


            await asyncio.sleep(1)

    def listen(self):
        loop = asyncio.get_event_loop()
        try:
            loop.run_until_complete(self.run())
        finally:
            loop.close()
