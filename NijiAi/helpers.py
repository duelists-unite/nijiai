from dataclasses import dataclass
from typing import List
import re
import asyncio

@dataclass
class Mode:
    MINMAX_LP: int
    MINMAX_DESTRUCTION: int
    def Verify_Mode():
        #to be implemented
        pass



ca = {"mathmech nabla":1 , "mathmech diablo":2 }

class ComboCreator(object):
    def __init__(self,cards) -> None:
        super().__init__()
        ccase = lambda s :  re.sub(r'\w+', lambda m:m.group(0).capitalize(), s).replace(" ","")
        @dataclass
        class card:
            name: str
            card : int
            location : int = 0
            targets : List[int] = 0

        for c in list(cards.keys()):
            setattr(self, ccase(c),card(c,cards[c]))
        
        self.combos = []
    
    def combo(self,combo):
        def wrapper():
            self.combos += [combo]
        return wrapper()
    
    async def execute(self):
        for fun in self.combos:
            await fun()


