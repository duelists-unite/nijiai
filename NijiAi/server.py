from struct import pack
import requests
from model import BotUrls, BotDetails, IdleState, IdleCommandState, Zones
from errors import *
import time
from base64 import b64encode


class Server(object):
    def __init__(self) -> None:
        super().__init__()
        self.url = BotUrls("http://localhost:9999", 0)
        self.is_created = False
        self.botdetails = None
        self.new_state = IdleCommandState(**{"CurrentStateCode": 0, "CurrentStateName": "None"})
        self.zones = Zones()

    def create_bot(self, deck="mirror"):
        botd = next(x for x in requests.get(self.url.BotList).json()["Bots"] if x["DeckName"] == "AI_Custom")
        self.botdetails = BotDetails(**botd)
        self.url = BotUrls("http://localhost:9999", self.botdetails.BotID)
        if not deck == "mirror":
            resp = requests.get(self.url.SetBotData + "/Ai_Custom/{deck}").json()
        else:
            deck = requests.get(self.url.CurrentDeck).json()
            resp = requests.get(self.url.SetBotData + f"/AI_Custom/{deck['Hash']}").json()
        if resp['Code'] != 200:
            raise RuntimeError(resp['Reason'])
        return True

    def get_idle_cmd(self):
        resp = requests.get(self.url.BotStateHeaders).json()
        return IdleCommandState(**resp)

    def get_state(self):
        resp = requests.get(self.url.BotState).json()
        return IdleState(**resp)

    def run_aux_command(self,data):
        resp = requests.get(self.url.SetBotResponse + f"/{data}").json()
        print(resp)
"""
    # incomplete due to formating 
    def summon_card(self, data, location):
        resp = requests.get(self.url.SetBotResponse + f"/{data}").json()
        if resp["Code"] == 200:
            if location == "auto":
                resp = requests.get(self.url.BotState).json()
                while resp["CurrentStateHeader"]["CurrentStateCode"] != 10:
                    resp = requests.get(self.url.BotState).json()
                    time.sleep(1)
                l = self.zones.available_zones(resp["CurrentStateData"]["Filter"])
                if len(l) > 0:
                    zone = pack('hhhh', l[0], 0, 0, 0)
                    zone = b64encode(zone).decode()
                    r = requests.get(self.url.SetBotResponse + f"/{zone}").json()
                    if r["Code"] == 200:
                        return True
                    else:
                        raise LocationSettingFailure(r["Reason"])
                else:
                    raise LocationSettingFailure("No Zone Avilable")
            else:
                zone = pack('hhhh', location, 0, 0, 0)
                zone = b64encode(zone).decode()
                r = requests.get(self.url.SetBotResponse + f"/{zone}").json()
                if r["Code"] == 200:
                    return True
                else:
                    raise LocationSettingFailure(r["Reason"])
        else:
            raise CardNotSummonableError(resp["Reason"])
"""