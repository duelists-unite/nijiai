

class CardNotSummonableError(Exception):
    pass

class LocationSettingFailure(Exception):
    pass

class CardCantSummon(Exception):
    pass

class WrongStateError(Exception):
    pass