from core import Bot

bot = Bot(deck="M+fcwXKNIfpBOdPZk9asIHxu0U8GGN6ce5kFht9eE2QE4YD7cswgfK/NEIwnH5vHCMInDDOYYdj3ewHD1YvKDJfPT2Z2ZJjNEO2qywSi1yvfA+PJe5oYbsy1ZwGJg2gQv6FWk/W96j+W/H2GLOadDMwguQuWBxgnS9uzdvVJswq2dDH1WLMy/WHTZ27dtJXpiIsia2PsGaYZ0irMt5VfMx/yd2Q4U/6dUar8LtPv29Is3vzVjNel5oOx2gQnZhgGmQkyH4RBdoL8DwA=")

@bot.register("on_idle")
async def print_bot_state():
    print(bot.state.CurrentStateName)
    for card in bot.data.CurrentStateData.SummonableCards:
            #auto select zone
            await bot.summon(card)
            #manual select
            #await bot.summon(card , bot.zones.z1)
            break
    await bot.to_battlephase()


if __name__ == "__main__":
    if bot.is_created:
        bot.listen()