
from NijiAi.core import Bot
from NijiAi.helpers import Mode

bot = Bot()
modes = Mode()


print(bot.is_created)
@bot.register("on_idle")
async def print_bot_state():
    print(bot.state.CurrentStateName)
    for card in bot.data.CurrentStateData.SummonableCards:
        if card.ID in [bot.cards.MathMechNabla]:
            #auto select zone
            await bot.summon(card)
            #manual select
            await bot.summon(card , bot.zones.z1)
            break
    await bot.end_phase()

@bot.register("on_battle")
async def battle():
    pass


if __name__ == "__main__":
    if bot.is_created:
        bot.listen()
